import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		int count = 0;
		int player = 0;
		
		while (count < 9) {
			
			System.out.print("Player " + (player + 1) + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player " + (player + 1) + " enter column number:");
			int col = reader.nextInt();
			
			if (row > 0 && row <= 3 && col > 0 && col <= 3 && board[row - 1][col - 1] == ' ') {
				
				if(player == 0) {
					board[row - 1][col - 1] = 'X';
				}else {
					board[row - 1][col - 1] = 'O';
				}
				
				count++;
				
				printBoard(board);
				
				if (checkboard(board, row -1 , col -1 )) {
					System.out.println("Player " + (player + 1) + " has won. Congratulations!");
					break;
				}
				
				player = (player + 1) % 2;
				
			}else{
				System.out.println("It is not a valid location. Try another one.");
			}

		}
		reader.close();
	}
	
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	public static boolean checkboard(char[][] board, int last_row, int last_col) {
		
		char symbol = board[last_row][last_col];

		boolean flag = true;
		for (int col = 0; col < 3; col++) {
			if (board[last_row][col] != symbol) {
				flag = false;
				break;
			}
		}
		if (flag) {
			return true;
		}

		flag = true;
		for (int row = 0; row < 3; row++) {
			if (board[row][last_col] != symbol) {
				flag = false;
				break;
			}
		}
		if (flag) {
			return true;
		}
		
		if (last_row==last_col) {
			flag = true;
			for (int loc =0; loc<3 ; loc++) {
				if (board[loc][loc] != symbol ) {
					flag = false;
					break;
				}
				
			}
			if (flag) {
				return true;
			}
		}
		
		if (last_row+last_col==2) {
			flag = true;
			for (int row =0; row<3 ; row++) {
				if (board[row][2-row] != symbol ) {
					flag = false;
					break;
				}		
			}
		}		
		
		return false;
	}

}
